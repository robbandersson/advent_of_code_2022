#
# Solution to Advent of Code 2002 Day  - 
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the  day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def compute():
    with open(data_dir + '/aoc_2022_.txt') as f:
        data = f.read().splitlines()
    # Part 1
    if problem == 1:        
        return
    # Part 2
    elif problem == 2:
        return 
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
