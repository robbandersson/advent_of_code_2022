import os
import re
import time
data_dir = os.path.normpath("data")

start_time = time.time()

with open(data_dir + '/aoc_2022_14.txt') as f:
    data = f.read().splitlines()

cave = set()
bottom_depth = None
for entry in data:
    coord = [int(s) for s in re.findall(r'\d+', entry)]
    num_coord = len(coord) // 2

    for k in range(num_coord - 1):
        x_start = coord[k * 2]
        y_start = coord[k * 2 + 1]
        x_end = coord[k * 2 + 2]
        y_end = coord[k * 2 + 3]

        if x_start == x_end:
            x = x_start
            if y_start > y_end:
                for y in range(y_end, y_start + 1):
                    cave.add((x, y))
            else:
                for y in range(y_start, y_end + 1):
                    cave.add((x, y))
        else:
            y = y_start
            if x_start > x_end:
                for x in range(x_end, x_start + 1):
                    cave.add((x, y))
            else:
                for x in range(x_start, x_end + 1):
                    cave.add((x, y))

        if not bottom_depth:
            bottom_depth = max(y_start, y_end)
        else:
            bottom_depth = max(bottom_depth, max(y_start, y_end))


def print_cave(c, s):
    x_min, x_max, y_min, y_max = 485, 515, 0, 11
    for y_curr in range(y_min, y_max + 1):
        row = ''
        for x_curr in range(x_min, x_max + 1):
            if (x_curr, y_curr) in c:
                row += '#'
            elif (x_curr, y_curr) in s:
                row += 'o'
            else:
                row += '.'
        print(row)


start_coord = (500, 0)
coord = start_coord
count = 0
sand = set()
# Part 1
# while True:
#     x, y = coord
#     if y >= bottom_depth:
#         break
#     if (x, y + 1) in cave.union(sand):
#         if (x - 1, y + 1) in cave.union(sand):
#             if (x + 1, y + 1) in cave.union(sand):
#                 sand.add(coord)
#                 count += 1
#                 coord = start_coord
#             else:
#                 coord = (x + 1, y + 1)
#         else:
#             coord = (x - 1, y + 1)
#     else:
#         coord = (x, y + 1)

# Part 2
x, y = coord
while True:
    if (x, y) in cave.union(sand):
        (x, y) = start_coord
    if (x, y + 1) not in cave.union(sand) and y < bottom_depth + 1:
        y += 1
    elif (x - 1, y + 1) not in cave.union(sand) and y < bottom_depth + 1:
        y += 1
        x -= 1
    elif (x + 1, y + 1) not in cave.union(sand) and y < bottom_depth + 1:
        y += 1
        x += 1
    else:
        print(count)
        count += 1
        sand.add((x, y))
    if (x, y) == start_coord:
        break

print(count)
print("%s seconds" % (time.time() - start_time))
