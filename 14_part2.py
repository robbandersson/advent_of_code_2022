import os
import numpy as np
import time
data_dir = os.path.normpath("data")

start_time = time.time()

delimiter = ' -> '
data = []
GRID_SIZE = 1000
with open(data_dir + '/aoc_2022_14.txt') as file:
    for line in file.readlines():
        element = []
        for token in line.strip().split(delimiter):
            element.append([int(x) for x in token.split(',')])
        data.append(element)

grid = np.zeros([GRID_SIZE, GRID_SIZE])

bottom_depth = 0
for line in data:
    x = line[0][0]
    y = line[0][1]
    grid[y, x] = 1
    for index in range(1, len(line)):
        dx = line[index][0] - line[index - 1][0]
        dy = line[index][1] - line[index - 1][1]
        for k in range(max(abs(dx), abs(dy))):
            x += (0 if dx == 0 else -1 if dx < 0 else 1)
            y += (0 if dy == 0 else -1 if dy < 0 else 1)
            grid[y, x] = 1
            bottom_depth = max(bottom_depth, y)

grid[bottom_depth + 2, :] = 1
# Add floor
(x, y) = (500, 0)
count = 0
while True:
    if grid[y, x] == 1:
        (x, y) = (500, 0)
    if grid[y + 1, x] == 0:
        y += 1
    elif grid[y + 1, x - 1] == 0:
        y += 1
        x -= 1
    elif grid[y + 1, x + 1] == 0:
        y += 1
        x += 1
    else:
        count += 1
        grid[y, x] = 1
    if (x, y) == (500, 0):
        break

print(count)
print("%s seconds" % (time.time() - start_time))
