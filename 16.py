import os
import networkx as nx
import re
import time
import itertools
data_dir = os.path.normpath("data")

start_time = time.time()

valves = {}
flow_rates = {}

with open(data_dir + '/aoc_2022_16.txt') as f:
    data = f.read().splitlines()

graph = nx.DiGraph()
for entry in data:
    tokens = entry.split(' ')

    valve = tokens[1]
    flow_rate = int(re.findall(r'\d+', entry)[0])
    neighbours = set([node for node in ''.join(tokens[9:]).split(',')])

    valves[valve] = neighbours
    flow_rates[valve] = flow_rate

    graph.add_node(valve)
    for node in neighbours:
        graph.add_edge(valve, node)

# Find shortest paths
distances = {}
for source in graph.nodes():
    for target in graph.nodes():
        if source != target:
            key = ''.join(sorted([source, target]))
            if key not in distances:
                # Add 1 to the distance, as it takes one minute to open the valve
                distances[key] = nx.shortest_path_length(graph, source=source, target=target) + 1

# Extract the valves with flow rates that are not zero
valves_to_open = [x for x in valves.keys() if flow_rates[x] != 0]

start_location = 'AA'
time_limit = 26

pressure_by_path = {}
pressure_by_permutation = {}
path_permutations = {}

# Stack of current branch - [path], minutes elapsed, { valve: <minute opened> }
stack = [[[start_location], 0, {}]]
while stack:
    path, minutes_elapsed, open_valves = stack.pop()
    current_valve = path[-1]

    released_pressure = 0
    for valve, minute_opened in open_valves.items():
        released_pressure += max(time_limit - minute_opened, 0) * flow_rates[valve]

    path_key = ''.join(path)
    pressure_by_path[path_key] = released_pressure

    path_key_lexi = ''.join(sorted(path))
    path_permutations.setdefault(path_key_lexi, [])
    path_permutations[path_key_lexi].append(path_key)

    pressure_by_permutation.setdefault(path_key_lexi, 0)
    pressure_by_permutation[path_key_lexi] = max(pressure_by_permutation[path_key_lexi], released_pressure)

    if minutes_elapsed >= time_limit or len(path) == len(valves_to_open) + 1:
        continue
    else:
        for next_valve in set(valves_to_open).difference(set(open_valves.keys())):
            key = ''.join(sorted([path[-1], next_valve]))
            travel_time = distances[key]

            new_minutes_elapsed = minutes_elapsed + travel_time

            new_path = path.copy()
            new_path.append(next_valve)

            new_open_valves = open_valves.copy()
            new_open_valves[next_valve] = new_minutes_elapsed

            stack.append([new_path, new_minutes_elapsed, new_open_valves])

maximum_release = 0
for k in range(1, len(valves_to_open) // 2 + 1):
    print(k)
    for perm in itertools.combinations(valves_to_open, k):
        agent_1_valves = list(perm)
        agent_1_key = ''.join(sorted(agent_1_valves + [start_location]))
        if agent_1_key in pressure_by_permutation:
            agent_1_release = pressure_by_permutation[agent_1_key]
        else:
            agent_1_release = 0
        agent_2_valves = list(set(valves_to_open).difference(set(agent_1_valves)))
        agent_2_key = ''.join(sorted(agent_2_valves + [start_location]))
        if agent_2_key in pressure_by_permutation:
            agent_2_release = pressure_by_permutation[agent_2_key]
        else:
            agent_2_release = 0
        maximum_release = max(maximum_release, agent_1_release + agent_2_release)

print(maximum_release)
print("%s seconds" % (time.time() - start_time))
