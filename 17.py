import os
import numpy as np
import time
data_dir = os.path.normpath("data")

start_time = time.time()

with open(data_dir + '/aoc_2022_17.txt') as file:
    data = file.read().splitlines()

GRID_SIZE = 500000
LEFT_EDGE = 0
RIGHT_EDGE = 7
grid = np.zeros([GRID_SIZE, RIGHT_EDGE])
grid[GRID_SIZE - 1, :] = 1


def print_grid(grid, upper):
    for k in range(upper + 2, 0, -1):
        row = '|'
        for val in grid[GRID_SIZE - k - 1, :]:
            row += str(int(val))
        row += '|'
        print(row)
    print('_' * 9)


def update_rock_position(rock_pos, direction):
    upd_rock_pos = list()
    if direction == '<':
        furthest_left = min([x[1] for x in rock_pos])
        if furthest_left == LEFT_EDGE:
            upd_rock_pos = rock_pos
        else:
            for pos in rock_pos:
                upd_rock_pos.append((pos[0], pos[1] - 1))
    elif direction == '>':
        furthest_right = max([x[1] for x in rock_pos])
        if furthest_right == RIGHT_EDGE - 1:
            upd_rock_pos = rock_pos
        else:
            for pos in rock_pos:
                upd_rock_pos.append((pos[0], pos[1] + 1))
    elif direction == 'v':
        for pos in rock_pos:
            upd_rock_pos.append((pos[0] + 1, pos[1]))

    return upd_rock_pos


tot_rocks = 20000
#tot_rocks = 1068
fallen_rocks = 0
rock_order = ['-', '+', 'L', 'I', 'O']
next_rock = 0
highest = GRID_SIZE - 1
jet_index = 0
last_fallen_rocks = 0
last_height = 0
while fallen_rocks < tot_rocks:
    row, col = (highest - 4, 2)
    landed = False
    push = True
    rock_pos = []
    if rock_order[next_rock] == '-':
        rock_pos = [(row, col), (row, col + 1), (row, col + 2), (row, col + 3)]
    elif rock_order[next_rock] == '+':
        rock_pos = [(row, col + 1), (row - 1, col + 1), (row - 2, col + 1), (row - 1, col), (row - 1, col + 2)]
    elif rock_order[next_rock] == 'L':
        rock_pos = [(row, col), (row, col + 1), (row, col + 2), (row - 1, col + 2), (row - 2, col +2)]
    elif rock_order[next_rock] == 'I':
        rock_pos = [(row, col), (row - 1, col), (row - 2, col), (row - 3, col)]
    elif rock_order[next_rock] == 'O':
        rock_pos = [(row, col), (row - 1, col), (row, col + 1), (row - 1, col + 1)]

    # for y in range(highest - 7, GRID_SIZE):
    #     p = '|'
    #     for x in range(0, 7):
    #         if (y, x) in rock_pos:
    #             p += '@'
    #         elif x < 7:
    #             if grid[y, x] == 1:
    #                 p += '#'
    #             else:
    #                 p += '.'
    #         else:
    #             p += '.'
    #     p += '|'
    #     print(p)
    # print('_'*9)

    while not landed:
        if push:
            if jet_index == len(data[0]):
                max_height_rock = max([x[0] for x in rock_pos])
                rocks_delta = fallen_rocks - last_fallen_rocks
                height_delta = GRID_SIZE - highest - 1 - last_height
                print('Rock form:', rock_order[next_rock], 'Number of fallen rocks:', fallen_rocks, 'Since last',
                      rocks_delta, 'Height:', GRID_SIZE - highest - 1, 'since last:', height_delta, 'left to bottom',
                      highest - max_height_rock)
                last_height = GRID_SIZE - highest - 1
                last_fallen_rocks = fallen_rocks
                jet_index = 0
            upd_rock_pos = update_rock_position(rock_pos, data[0][jet_index])
            for pos in upd_rock_pos:
                if grid[pos[0], pos[1]] == 1:
                    upd_rock_pos = rock_pos
            jet_index += 1
            push = False
        else:
            upd_rock_pos = update_rock_position(rock_pos, 'v')
            push = True
        for pos in upd_rock_pos:
            if grid[pos[0], pos[1]] == 1:
                landed = True
                break

        if landed:
            for pos in rock_pos:
                grid[pos[0], pos[1]] = 1
                highest = min(highest, pos[0])
            fallen_rocks += 1
        rock_pos = upd_rock_pos

    if next_rock == len(rock_order) - 1:
        next_rock = 0
    else:
        next_rock += 1

print(GRID_SIZE - highest - 1)
#print_grid(grid, 20)