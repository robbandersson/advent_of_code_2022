import os
import math
data_dir = os.path.normpath("data")

with open(data_dir + '/aoc_2022_17.txt') as file:
    data = file.read().splitlines()

jet_directions = list(map(lambda _: -1 if _ == '<' else 1, data[0]))
solid_squares = set([(x, 0) for x in range(7)])
placed_rocks = []
X_START_OFFSET = 2
Y_START_OFFSET = 4


def spawn_rock(tower_height, pattern):
    x, y = (X_START_OFFSET, tower_height + Y_START_OFFSET)

    if pattern == 0:
        return {(x, y),
                (x + 1, y),
                (x + 2, y),
                (x + 3, y)}
    elif pattern == 1:
        return {(x + 1, y),
                (x, y + 1),
                (x + 1, y + 1),
                (x + 2, y + 1),
                (x + 1, y + 2)}
    elif pattern == 2:
        return {(x, y),
                (x + 1, y),
                (x + 2, y),
                (x + 2, y + 1),
                (x + 2, y + 2)}
    elif pattern == 3:
        return {(x, y),
                (x, y + 1),
                (x, y + 2),
                (x, y + 3)}
    else:
        return {(x, y),
                (x, y + 1),
                (x + 1, y),
                (x + 1, y + 1)}


def should_push(rock, direction):
    for square in rock:
        x, y = square

        if direction == -1 and x - 1 < 0:
            return False

        if direction == 1 and x + 1 > 6:
            return False

        if (x + direction, y) in solid_squares:
            return False
    return True


def push(rock, direction):
    return set([(x + direction, y) for (x, y) in rock])


def should_fall(rock):
    for square in rock:
        x, y = square

        if (x, y-1) in solid_squares:
            return False
    return True


def fall(rock):
    return set([(x, y-1) for (x, y) in rock])


def come_to_rest(rock):
    max_y = 0

    for square in rock:
        _, y = square
        solid_squares.add(square)
        max_y = max(max_y, y)
    placed_rocks.append(rock)

    return (max_y, rock)


next_jet = 0
next_rock = 0
tower_height = 0
cycle_found = False
seen_states = {}
ROCKS_TO_FALL = 1000000000000
MINIMUM_CYCLE_LENGTH = 200
MAXIMUM_CYCLE_LENGTH = 210
r = 0

while r < ROCKS_TO_FALL:
    if r % 100 == 0:
        print(r)
    if not cycle_found:
        if r > MAXIMUM_CYCLE_LENGTH:
            # Create states containing of multiples of 5 of, between MINIMUM_CYCLE_LENGTH and MAXIMUM_CYCLE_LENGTH,
            # the last recorded rocks, and their relative placement. If a state has been seen before, assume a cycle
            # and remove a multiple of that cycle from the remaining rocks, add the multiple times the cycle height
            # to the current tower and continue with the remaining rocks
            for k in range(MINIMUM_CYCLE_LENGTH, MAXIMUM_CYCLE_LENGTH, 5):
                last_n_rocks = list(map(
                    lambda rock: frozenset([(x, y - tower_height) for x, y in rock]),
                    placed_rocks[-k:]
                ))
                state = frozenset([
                    next_rock,
                    frozenset(last_n_rocks)
                ])

                if state in seen_states:
                    print('Found cycle of length', len(last_n_rocks), 'after', r, 'rocks.')
                    cycle_found = True
                    r0, height0 = seen_states[state]

                    cycle_length = r - r0
                    height_of_cycle = tower_height - height0
                    remaining_rocks = ROCKS_TO_FALL - r0
                    num_cycles = remaining_rocks // cycle_length

                    r = r0 + (cycle_length * num_cycles)
                    tower_height = height0 + (height_of_cycle * num_cycles)
                    for rock in last_n_rocks:
                        for x, y in rock:
                            solid_squares.add((x, y + tower_height))
                else:
                    seen_states[state] = (r, tower_height)

    rock = spawn_rock(tower_height, next_rock)
    while True:
        direction = jet_directions[next_jet]
        next_jet = (next_jet + 1) % len(jet_directions)

        if should_push(rock, direction):
            rock = push(rock, direction)

        if should_fall(rock):
            rock = fall(rock)
        else:
            break

    max_y, rock = come_to_rest(rock)
    tower_height = max(tower_height, max_y)
    next_rock = (next_rock + 1) % 5
    r += 1

print(tower_height)
