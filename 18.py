import os
import re
import time
data_dir = os.path.normpath("data")

start_time = time.time()
all_sides = []
all_cubes = []
global free_sides
free_sides = 0

with open(data_dir + '/aoc_2022_18.txt') as file:
    data = file.read().splitlines()


def add_cube(cube):
    sides = []
    (x, y, z) = cube
    all_cubes.append((x, y, z))

    sides.append((x + 1, y, z))
    sides.append((x - 1, y, z))
    sides.append((x, y + 1, z))
    sides.append((x, y - 1, z))
    sides.append((x, y, z + 1))
    sides.append((x, y, z - 1))

    for side in sides:
        global free_sides
        if side in all_cubes:
            free_sides -= 1
        else:
            free_sides += 1


def find_duplicates(l):
    seen = set()
    seen_2 = set()
    for item in l:
        if item in seen:
            seen_2.add(item)
        else:
            seen.add(item)
    return list(seen_2)


for entry in data:
    cube = [int(s) for s in re.findall(r'\d+', entry)]
    add_cube(cube)

print(free_sides)
print("%s seconds" % (time.time() - start_time))
