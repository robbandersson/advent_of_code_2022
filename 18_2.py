import os
import re
import time
data_dir = os.path.normpath("data")

start_time = time.time()
all_cubes = []
all_possible_adjacent_cubes = []
all_closed_in_cubes = []

global free_sides
free_sides = 0

with open(data_dir + '/aoc_2022_18.txt') as file:
    data = file.read().splitlines()


def add_cube(cube):
    possible_adjacent_cubes = []
    (x, y, z) = cube
    all_cubes.append((x, y, z))

    possible_adjacent_cubes.append((x + 1, y, z))
    possible_adjacent_cubes.append((x - 1, y, z))
    possible_adjacent_cubes.append((x, y + 1, z))
    possible_adjacent_cubes.append((x, y - 1, z))
    possible_adjacent_cubes.append((x, y, z + 1))
    possible_adjacent_cubes.append((x, y, z - 1))

    all_possible_adjacent_cubes.append(possible_adjacent_cubes)

    for adjacent_cube in possible_adjacent_cubes:
        global free_sides
        if adjacent_cube in all_cubes:
            free_sides -= 1
        else:
            free_sides += 1
            is_closed_in(adjacent_cube)


def is_closed_in(cube):
    possible_adjacent_cubes = []
    (x, y, z) = cube

    possible_adjacent_cubes.append((x + 1, y, z))
    possible_adjacent_cubes.append((x - 1, y, z))
    possible_adjacent_cubes.append((x, y + 1, z))
    possible_adjacent_cubes.append((x, y - 1, z))
    possible_adjacent_cubes.append((x, y, z + 1))
    possible_adjacent_cubes.append((x, y, z - 1))

    global free_sides
    closed_in = True

    for adjacent_cube in possible_adjacent_cubes:
        if adjacent_cube in all_cubes:
            continue
        else:
            closed_in = False
            break

    if closed_in and cube not in all_closed_in_cubes and cube not in all_cubes:
        free_sides -= 6
        all_closed_in_cubes.append(cube)


for entry in data:
    cube = [int(s) for s in re.findall(r'\d+', entry)]
    add_cube(cube)


print(free_sides)
print("%s seconds" % (time.time() - start_time))
