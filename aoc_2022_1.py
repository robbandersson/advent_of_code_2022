#
# Solution to Advent of Code 2002 Day 1 - Calorie Counting
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the first day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def compute():
    with open(data_dir + '/aoc_2022_1.txt') as f:
        data = f.read().splitlines()
        # Part 1
        if problem == 1:
            tot = 0
            pos = 1
            highest = 0
            for val in data:
                if val == '':
                    if tot > highest:
                        highest = tot
                        elf_pos = pos
                    tot = 0
                    pos += 1
                else:
                    tot += int(val)
            return highest
        # Part 2
        elif problem == 2:
            elf_cal = dict()
            tot = 0
            pos = 1
            for val in data:
                if val == "":
                    elf_cal[pos] = tot
                    tot = 0
                    pos += 1
                else:
                    tot += int(val)
            top = sum(dict(sorted(elf_cal.items(), key=lambda item: item[1], reverse=True)[:3]).values())
            return top
        else:
            return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
