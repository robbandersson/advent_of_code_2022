#
# Solution to Advent of Code 2002 Day 10 - Cathode-Ray Tube
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
import re

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the tenth day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def draw_pixel(cycle, sprite_pos):
    cycle = cycle % 40
    if (cycle >= sprite_pos - 1) and (cycle <= sprite_pos + 1):
        return '#'
    else:
        return '.'


def compute():
    with open(data_dir + '/aoc_2022_10.txt') as f:
        data = f.read().splitlines()
    # Part 1
    if problem == 1:
        cycle = 0
        X = 1
        register = dict()
        for entry in data:
            if entry.startswith('no'):
                cycle += 1
                register[cycle] = X
            elif entry.startswith('addx -'):
                cycle += 1
                register[cycle] = X
                cycle += 1
                register[cycle] = X
                X -= int(re.findall(r'[0-9]+', entry)[0])
            else:
                cycle += 1
                register[cycle] = X
                cycle += 1
                register[cycle] = X
                X += int(re.findall(r'[0-9]+', entry)[0])
        check_cycle = 20
        tot = 0
        while check_cycle < 221:
            tot += check_cycle * register[check_cycle]
            check_cycle += 40
        return tot
    # Part 2
    elif problem == 2:
        pixels = ''
        cycle = 0
        sprite_pos = 1
        for entry in data:
            if entry.startswith('no'):
                cycle += 1
                pixels += draw_pixel(cycle - 1, sprite_pos)
            elif entry.startswith('addx -'):
                cycle += 1
                pixels += draw_pixel(cycle - 1, sprite_pos)
                cycle += 1
                pixels += draw_pixel(cycle - 1, sprite_pos)
                sprite_pos -= int(re.findall(r'[0-9]+', entry)[0])
            else:
                cycle += 1
                pixels += draw_pixel(cycle - 1, sprite_pos)
                cycle += 1
                pixels += draw_pixel(cycle - 1, sprite_pos)
                sprite_pos += int(re.findall(r'[0-9]+', entry)[0])
        start_point = 0
        end_point = 40
        while end_point <= len(pixels):
            print(pixels[start_point:end_point])
            start_point = end_point
            end_point += 40
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
