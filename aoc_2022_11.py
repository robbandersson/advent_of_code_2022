#
# Solution to Advent of Code 2002 Day 11 - Monkey in the Middle
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
import re
import heapq

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the eleventh day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def compute():
    with open(data_dir + '/aoc_2022_11.txt') as f:
        data = f.read().splitlines()

    items = dict()
    operation = dict()
    rule = dict()
    inspections = dict()
    monkey = 0
    for entry in data:
        if entry.startswith('Monkey'):
            monkey = int(re.findall(r'[0-9]+', entry)[0])
            inspections[monkey] = 0
        elif entry.__contains__('Starting'):
            items[monkey] = [int(s) for s in re.findall(r'[0-9]+', entry)]
        elif entry.__contains__('Operation'):
            if entry.__contains__('+'):
                operation[monkey] = {'operator': 'plus', 'value': int(re.findall(r'[0-9]+', entry)[0])}
            elif any(map(str.isdigit, entry)):
                operation[monkey] = {'operator': 'multi', 'value': int(re.findall(r'[0-9]+', entry)[0])}
            else:
                operation[monkey] = {'operator': 'multi', 'value': 'self'}
        elif entry.__contains__('Test'):
            rule[monkey] = {'div': int(re.findall(r'[0-9]+', entry)[0])}
        elif entry.__contains__('true'):
            rule[monkey]['true'] = int(re.findall(r'[0-9]+', entry)[0])
        elif entry.__contains__('false'):
            rule[monkey]['false'] = int(re.findall(r'[0-9]+', entry)[0])

    # Part 1
    if problem == 1:
        worry_div = 3
        tot_num_rounds = 20
        current_round = 0
        while current_round < tot_num_rounds:
            for monkey in items.keys():
                for item in items[monkey]:
                    inspections[monkey] = inspections[monkey] + 1
                    # Operation
                    if operation[monkey]['operator'] == 'plus':
                        val = item + operation[monkey]['value']
                    else:
                        if operation[monkey]['value'] == 'self':
                            val = item * item
                        else:
                            val = item * operation[monkey]['value']
                    # New worry level
                    val = val // worry_div
                    if val % rule[monkey]['div'] == 0:
                        items[rule[monkey]['true']].append(val)
                    else:
                        items[rule[monkey]['false']].append(val)
                items[monkey] = []
            current_round += 1
        highest = sorted(inspections.items(), key=lambda pair: pair[1], reverse=True)[:2]
        return highest[0][1] * highest[1][1]

    # Part 2
    elif problem == 2:
        lcm = 1
        for monkey in rule.keys():
            lcm = lcm * rule[monkey]['div']
        worry_div = lcm
        tot_num_rounds = 10000
        current_round = 0
        while current_round < tot_num_rounds:
            for monkey in items.keys():
                for item in items[monkey]:
                    inspections[monkey] = inspections[monkey] + 1
                    # Operation
                    if operation[monkey]['operator'] == 'plus':
                        val = item + operation[monkey]['value']
                    else:
                        if operation[monkey]['value'] == 'self':
                            val = item * item
                        else:
                            val = item * operation[monkey]['value']
                    # New worry level
                    val = val % worry_div
                    if val % rule[monkey]['div'] == 0:
                        items[rule[monkey]['true']].append(val)
                    else:
                        items[rule[monkey]['false']].append(val)
                items[monkey] = []
            current_round += 1
        highest = sorted(inspections.items(), key=lambda pair: pair[1], reverse=True)[:2]
        return highest[0][1] * highest[1][1]
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
