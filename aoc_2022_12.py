#
# Solution to Advent of Code 2002 Day 12 - Hill CLimbing Algorithm
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
import sys
sys.setrecursionlimit(1500)


data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the twelfth day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def traverse_map(curr_pos):
    (row, col) = curr_pos
    positions = [(row - 1, col), (row + 1, col), (row, col - 1), (row, col + 1)]
    for new_pos in positions:
        if new_pos in height_map.keys():
            # See if we can reach that position
            if height_map[new_pos] <= height_map[curr_pos] + 1:
                # Have we reached it before?
                if new_pos in distance.keys():
                    if distance[new_pos] > distance[curr_pos] + 1:
                        distance[new_pos] = distance[curr_pos] + 1
                        traverse_map(new_pos)
                else:
                    distance[new_pos] = distance[curr_pos] + 1
                    traverse_map(new_pos)


def reverse_traverse_map(curr_pos):
    (row, col) = curr_pos
    positions = [(row - 1, col), (row + 1, col), (row, col - 1), (row, col + 1)]
    for new_pos in positions:
        if new_pos in height_map.keys():
            # See if we can reach that position
            if height_map[new_pos] >= height_map[curr_pos] - 1:
                # Have we reached it before?
                if new_pos in rev_distance.keys():
                    if rev_distance[new_pos] > rev_distance[curr_pos] + 1:
                        rev_distance[new_pos] = rev_distance[curr_pos] + 1
                        reverse_traverse_map(new_pos)
                else:
                    rev_distance[new_pos] = rev_distance[curr_pos] + 1
                    reverse_traverse_map(new_pos)

def compute():
    with open(data_dir + '/aoc_2022_12.txt') as f:
        data = f.read().splitlines()

    global height_map
    global distance
    global rev_distance
    height_map = dict()
    distance = dict()
    rev_distance = dict()

    possible_starts = list()
    row = 0
    for entry in data:
        col = 0
        for val in entry:
            if val == 'S':
                start_pos = (row, col)
                distance[start_pos] = 0
                val = 'a'
                possible_starts.append((row, col))
            elif val == 'a':
                possible_starts.append((row, col))
            elif val == 'E':
                end_pos = (row, col)
                rev_distance[end_pos] = 0
                val = 'z'
            height_map[(row, col)] = ord(val) - 96
            col += 1
        row += 1
    # Part 1
    if problem == 1:
        traverse_map(start_pos)
        return distance[end_pos]
    # Part 2
    elif problem == 2:
        reverse_traverse_map(end_pos)
        shortest_dist = None
        shortest_pos = None
        for start_pos in possible_starts:
            if start_pos in rev_distance.keys():
                if shortest_dist:
                    if rev_distance[start_pos] < shortest_dist:
                        shortest_dist = rev_distance[start_pos]
                        shortest_pos = start_pos
                else:
                    shortest_dist = rev_distance[start_pos]
        return shortest_dist, shortest_pos
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
