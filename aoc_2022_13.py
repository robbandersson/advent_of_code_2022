#
# Solution to Advent of Code 2002 Day 13 - Distress Signal
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
import ast

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the thirteenth day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def compare(a, b):
    if isinstance(a, list) and isinstance(b, int):
        b = [b]
    elif isinstance(a, int) and isinstance(b, list):
        a = [a]

    if isinstance(a, int) and isinstance(b, int):
        if a < b:
            return 1
        elif a == b:
            return 0
        else:
            return -1

    if isinstance(a, list) and isinstance(b, list):
        i = 0
        while i < len(a) and i < len(b):
            comp = compare(a[i], b[i])
            if comp == 1:
                return 1
            elif comp == -1:
                return -1
            i += 1

        if len(a) < len(b):
            return 1
        elif len (a) == len(b):
            return 0
        else:
            return -1


def compute():
    with open(data_dir + '/aoc_2022_13.txt') as f:
        data = f.read().splitlines()

    # Part 1
    tot = 0
    idx = 0
    if problem == 1:
        a, b = None, None
        for entry in data:
            if a is None:
                a = eval(entry)
                idx += 1
            elif b is None:
                b = eval(entry)
            else:
                if compare(a, b) == 1:
                    tot += idx
                a, b = None, None
        return tot
    # Part 2
    elif problem == 2:
        ordered = [eval(data[0])]
        for k in range(1, len(data)):
            if data[k] != '':
                b = eval(data[k])
                j = 0
                while j < len(ordered):
                    a = ordered[j]
                    if compare(a, b) == -1:
                        break
                    j += 1
                if j < len(ordered):
                    ordered.insert(j, b)
                else:
                    ordered.append(b)

        start_signal = [[2]]
        j = 0
        while j < len(ordered):
            a = ordered[j]
            if compare(a, start_signal) == -1:
                break
            j += 1
        if j < len(ordered):
            ordered.insert(j, start_signal)
        else:
            ordered.append(start_signal)

        end_signal = [[6]]
        j = 0
        while j < len(ordered):
            a = ordered[j]
            if compare(a, end_signal) == -1:
                break
            j += 1
        if j < len(ordered):
            ordered.insert(j, end_signal)
        else:
            ordered.append(end_signal)

        return (ordered.index(start_signal) + 1) * (ordered.index(end_signal) + 1)

    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
