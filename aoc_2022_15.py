#
# Solution to Advent of Code 2002 Day 15 - Beacon Exclusion Zone
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import re
import numpy as np
data_dir = os.path.normpath("data")


def find_manhattan_dist(a, b):
    return np.abs(a[0] - b[0]) + np.abs(a[1] - b[1])


def points_dist_away(p, d):
    x, y = p[0], p[1]
    upper_right = list(zip(range(x, x + d + 1), range(y + d + 1, y, -1)))
    lower_right = list(zip(range(x + d + 1, x, -1), range(y, y - d - 1, -1)))
    lower_left = list(zip(range(x, x - d - 1, -1), range(y - d - 1, y)))
    upper_left = list(zip(range(x - d - 1, x), range(y, y + d + 1)))
    return upper_right + lower_right + upper_left + lower_left


def compute():
    with open(data_dir + '/aoc_2022_15.txt') as f:
        data = f.read().splitlines()
    part = 2
    # Read sensors and beacons
    beacons = set()
    sensors = set()
    for row in data:
        coord = [int(s) for s in re.findall(r'-?\d+', row)]
        sensors.add((coord[0], coord[1]))
        beacons.add((coord[2], coord[3]))

    # Find closest beacon to each sensor
    closest_beacon = dict()
    sens_pos = list()
    sens_dist = list()
    for sensor in sensors:
        for beacon in beacons:
            dist = find_manhattan_dist(sensor, beacon)
            if sensor in closest_beacon.keys():
                if dist < closest_beacon[sensor]['distance']:
                    closest_beacon[sensor]['distance'] = dist
                    closest_beacon[sensor]['beacon'] = beacon
            else:
                closest_beacon[sensor] = {'distance': dist, 'beacon': beacon}
        sens_pos.append(sensor)
        sens_dist.append(dist)

    if part == 1:
        target_row = 2000000
        covered_in_row = list()
        for sensor, values in closest_beacon.items():
            print(sensor)
            # Find distance to target row
            target_row_dist = np.abs(sensor[1] - target_row)
            if target_row_dist > values['distance']:
                continue
            else:
                x_deviation = 0
                while target_row_dist <= values['distance']:
                    covered_in_row.append((sensor[0] + x_deviation, target_row))
                    covered_in_row.append((sensor[0] - x_deviation, target_row))
                    x_deviation += 1
                    target_row_dist = find_manhattan_dist(sensor, (sensor[0] + x_deviation, target_row))

        covered = set(covered_in_row).difference(beacons)
        return len(covered)
    else:
        edge_points = list()
        sens = list()
        dist = list()
        for sensor, values in closest_beacon.items():
            print(sensor)
            edge_points += points_dist_away(sensor, values['distance'])
            sens.append(sensor)
            dist.append(values['distance'])

        lower = 0
        upper = 4000000
        for p in edge_points:
            x, y = p[0], p[1]
            if lower <= x <= upper and lower <= y <= upper:
                if (np.abs(np.array(sens) - np.array([x, y])).sum(axis=1) > np.array(dist)).all():
                    return x * 4000000 + y


if __name__ == '__main__':
    print(compute())
