#
# Solution to Advent of Code 2022 Day 16 - Proboscidea Volacnium
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
import networkx as nx
import re
from itertools import permutations
import time

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the sixteenth day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def analyze_path(start, release_time, path, visited):
    if release_time + 1 < 30 and sorted(visited) != sorted(flow_nodes):
        candidates = [i for i in flow_nodes if i not in visited]
        for node in candidates:
            dist = distances[(start, node)]
            cont_path = path + [(release_time + dist + 1, flow_rates[node])]
            cont_visited = visited + [node]
            analyze_path(node, release_time+1+dist, cont_path, cont_visited)
    else:
        released_flow = 0
        for entry in path:
            released_flow += (30 - entry[0]) * entry[1]
        global max_release
        if released_flow > max_release:
            max_release = released_flow


def find_best_path(nodes):
    global max_flow
    max_flow = 0
    start = 'AA'
    for node in nodes:
        release_time = distances[(start, node)] + 1
        path = [(release_time, flow_rates[node])]
        visited = [node]
        analyze_path_2(node, release_time, path, visited, nodes)
    return max_flow


def analyze_path_2(start, release_time, path, visited, all_nodes):
    if release_time < 26 and sorted(visited) != sorted(all_nodes):
        candidates = [node for node in all_nodes if node not in visited]
        for node in candidates:
            dist = distances[(start, node)]
            new_release_time = release_time + dist + 1
            cont_path = path + [(new_release_time, flow_rates[node])]
            cont_visited = visited + [node]
            analyze_path_2(node, new_release_time, cont_path, cont_visited, all_nodes)
    else:
        released_flow = 0
        for entry in path:
            released_flow += (26 - entry[0]) * entry[1]
        global max_flow
        if released_flow > max_flow:
            max_flow = released_flow


def compute():
    with open(data_dir + '/aoc_2022_16.txt') as f:
        data = f.read().splitlines()

    global flow_rates
    global flow_nodes
    global distances
    global max_release
    flow_rates = dict()
    flow_nodes = list()
    distances = dict()
    max_release = 0

    # Create a directed graph
    G = nx.DiGraph()
    for entry in data:
        nodes = re.findall('[A-Z][A-Z]', entry)
        start = nodes[0]
        flow = [int(s) for s in re.findall('[0-9]+', entry)][0]
        flow_rates[start] = flow
        if flow != 0:
            flow_nodes.append(start)
        G.add_node(start)
        for k in range(1, len(nodes)):
            G.add_node(nodes[k])
            G.add_edge(start, nodes[k])

    # Find shortest path between all nodes
    nodes = list(G.nodes())
    for start_node in nodes:
        for end_node in nodes:
            if start_node != end_node:
                distances[(start_node, end_node)] = nx.shortest_path_length(G, source=start_node, target=end_node)

    # Part 1
    if problem == 1:
        for node in flow_nodes:
            arrival_time = distances[('AA', node)]
            path = [(arrival_time + 1, flow_rates[node])]
            visited = [node]
            analyze_path(start=node, release_time=arrival_time + 1, path=path, visited=visited)
        return max_release
    # Part 2
    elif problem == 2:
        start_time = time.time()
        max_release = 0
        analyzed_permutation = set()
        for k in range(1, len(flow_nodes) // 2 + 1):
            print(k)
            perm_agent_1 = permutations(flow_nodes, k)
            for perm_1 in perm_agent_1:
                key = ''.join([str(elem) for elem in sorted(perm_1)])
                if key in analyzed_permutation:
                    continue
                else:
                    perm_2 = list(set(flow_nodes).difference(set(perm_1)))
                    flow_1 = find_best_path(perm_1)
                    flow_2 = find_best_path(perm_2)
                    analyzed_permutation.add(key)
                    if flow_1 + flow_2 > max_release:
                        max_release = flow_1 + flow_2
        print("%s seconds" % (time.time() - start_time))
        return max_release
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
