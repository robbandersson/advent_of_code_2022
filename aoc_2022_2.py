#
# Solution to Advent of Code 2002 Day 2 - Rock Paper Scissors
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the second day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def outcome_2(hand_1, hand_2):
    if hand_1 == 'A':
        if hand_2 == 'X':
            return 0 + 3
        elif hand_2 == 'Y':
            return 3 + 1
        else:
            return 6 + 2
    elif hand_1 == 'B':
        if hand_2 == 'X':
            return 0 + 1
        elif hand_2 == 'Y':
            return 3 + 2
        else:
            return 6 + 3
    else:
        if hand_2 == 'X':
            return 0 + 2
        elif hand_2 == 'Y':
            return 3 + 3
        else:
            return 6 + 1


def outcome(hand_1, hand_2):
    if hand_1 == 'A':
        if hand_2 == 'X':
            return 1 + 3
        elif hand_2 == 'Y':
            return 2 + 6
        else:
            return 3 + 0
    elif hand_1 == 'B':
        if hand_2 == 'X':
            return 1 + 0
        elif hand_2 == 'Y':
            return 2 + 3
        else:
            return 3 + 6
    else:
        if hand_2 == 'X':
            return 1 + 6
        elif hand_2 == 'Y':
            return 2 + 0
        else:
            return 3 + 3


def compute():
    with open(data_dir + '/aoc_2022_2.txt') as f:
        data = f.read().splitlines()
    # Part 1
    if problem == 1:
        tot = 0
        for game in data:
            hand_1 = game[0]
            hand_2 = game[2]
            tot += outcome(hand_1, hand_2)
        return tot
    # Part 2
    elif problem == 2:
        tot = 0
        for game in data:
            hand_1 = game[0]
            hand_2 = game[2]
            tot += outcome_2(hand_1, hand_2)
        return tot
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())