#
# Solution to Advent of Code 2002 Day 3 - Rucksack Reorganization
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the third day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def find_highest_common_item(comp_1, comp_2):
    highest = 0
    for item in comp_1:
        if item in comp_2:
            if item.isupper():
                val = ord(item) - 64 + 26
            else:
                val = ord(item) - 96
            if val > highest:
                highest = val
    return highest


def find_badge(ruck_1, ruck_2, ruck_3):
    for item in ruck_1:
        if item in ruck_2 and item in ruck_3:
            if item.isupper():
                val = ord(item) - 64 + 26
            else:
                val = ord(item) - 96
    return val


def compute():
    with open(data_dir + '/aoc_2022_3.txt') as f:
        data = f.read().splitlines()
    # Part 1
    if problem == 1:
        tot = 0
        for val in data:
            comp_1 = val[:len(val) // 2]
            comp_2 = val[len(val) // 2:]
            tot += find_highest_common_item(comp_1, comp_2)
        return tot
    # Part 2
    elif problem == 2:
        tot = 0
        for k in range(len(data)//3):
            tot += find_badge(data[k * 3], data[k * 3 + 1], data[k * 3 + 2])
        return tot
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())