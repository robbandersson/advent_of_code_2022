#
# Solution to Advent of Code 2002 Day 4 - Camp Cleanup
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
import re

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the fourth day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def compute():
    with open(data_dir + '/aoc_2022_4.txt') as f:
        data = f.read().splitlines()
    # Part 1
    if problem == 1:
        tot = 0
        for entry in data:
            nums = [int(s) for s in re.findall(r'[0-9]+', entry)]
            if ((nums[0] <= nums[2]) and (nums[1] >= nums[3])) or ((nums[0] >= nums[2]) and (nums[1] <= nums[3])):
                tot += 1
        return tot
    # Part 2
    elif problem == 2:
        tot = 0
        for entry in data:
            nums = [int(s) for s in re.findall(r'[0-9]+', entry)]
            if (nums[0] <= nums[2]) and (nums[1] >= nums[2]):
                tot += 1
            elif (nums[2] <= nums[0]) and (nums[3] >= nums[0]):
                tot += 1
        return tot
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
