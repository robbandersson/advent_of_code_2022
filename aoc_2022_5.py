#
# Solution to Advent of Code 2002 Day 5 - Supply Stacks
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
import re

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the fifth day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def compute():
    with open(data_dir + '/aoc_2022_5.txt') as f:
        data = f.read().splitlines()
    # Part 1
    if problem == 1:
        crates = dict()
        row = 0
        for entry in data:
            if any(char.isdigit() for char in entry):
                break
            row += 1

        pos = []
        for crate_num in [int(s) for s in re.findall(r'[0-9]+', data[row])]:
            pos.append(data[row].index(str(crate_num)))

        for i, crate_num in enumerate([int(s) for s in re.findall(r'[0-9]+', data[row])]):
            temp_str = ''
            for k in range(row):
                sub_str = data[row - k - 1][pos[i]]
                if sub_str == ' ':
                    break
                else:
                    temp_str += sub_str
            crates[crate_num] = temp_str

        for next_row in range(row, len(data)):
            if data[next_row].startswith('move'):
                nums = [int(s) for s in re.findall(r'[0-9]+', data[next_row])]
                num_to_move = nums[0]
                move_from = nums[1]
                move_to = nums[2]
                for i in range(num_to_move):
                    crates[move_to] = crates[move_to] + crates[move_from][-1]
                    crates[move_from] = crates[move_from][:-1]
        on_top = ''
        for key, val in crates.items():
            on_top += val[-1]
        return on_top
    # Part 2
    elif problem == 2:
        crates = dict()
        row = 0
        for entry in data:
            if any(char.isdigit() for char in entry):
                break
            row += 1

        pos = []
        for crate_num in [int(s) for s in re.findall(r'[0-9]+', data[row])]:
            pos.append(data[row].index(str(crate_num)))

        for i, crate_num in enumerate([int(s) for s in re.findall(r'[0-9]+', data[row])]):
            temp_str = ''
            for k in range(row):
                sub_str = data[row - k - 1][pos[i]]
                if sub_str == ' ':
                    break
                else:
                    temp_str += sub_str
            crates[crate_num] = temp_str

        for next_row in range(row, len(data)):
            if data[next_row].startswith('move'):
                nums = [int(s) for s in re.findall(r'[0-9]+', data[next_row])]
                num_to_move = nums[0]
                move_from = nums[1]
                move_to = nums[2]
                crates[move_to] = crates[move_to] + crates[move_from][-num_to_move:]
                crates[move_from] = crates[move_from][:-num_to_move]
        on_top = ''
        for key, val in crates.items():
            on_top += val[-1]
        return on_top
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
