#
# Solution to Advent of Code 2002 Day 6 - Tuning Trouble
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the sixth day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def compute():
    with open(data_dir + '/aoc_2022_6.txt') as f:
        data = f.read().splitlines()
    # Part 1
    if problem == 1:
        found_first_marker = False
        pos = 0
        while not found_first_marker:
            sub_str = data[0][pos:pos+4]
            if len("".join(set(sub_str))) == 4:
                found_first_marker = True
            pos += 1
        return pos + 3
    # Part 2
    elif problem == 2:
        found_first_msg = False
        pos = 0
        while not found_first_msg:
            sub_str = data[0][pos:pos + 14]
            if len("".join(set(sub_str))) == 14:
                found_first_msg = True
            pos += 1
        return pos + 13
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
