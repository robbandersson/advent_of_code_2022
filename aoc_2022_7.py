#
# Solution to Advent of Code 2002 Day 7 -  No Space Left On Device
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
import re
from collections import defaultdict

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the seventh day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def compute():
    with open(data_dir + '/aoc_2022_7.txt') as f:
        data = f.read().splitlines()
    tree_struct = defaultdict()
    key = (0, )
    parent_key = None
    num_child = 0
    first = True
    for entry in data:
        if entry.startswith('$ cd ..'):
            key = tree_struct[key]['parent']
        elif entry.startswith('$ cd'):
            current_dir = entry[5:]
            if first:
                first = False
            else:
                parent_key = key
                # Find new key
                idx = tree_struct[key]['children_names'].index(current_dir)
                key = key + (idx, )
            # Initiate new entry in the tree structure
            tree_struct[key] = {'name': current_dir, 'children': [], 'children_names': [],
                                'parent': parent_key, 'memory': 0}
        elif entry.startswith('$ ls'):
            num_child = 0
        else:
            if entry[0].isdigit():
                tree_struct[key]['memory'] += int(re.findall(r'[0-9]+', entry)[0])
            else:
                sub_dir = entry[4:]
                tree_struct[key]['children'].append(key + (num_child, ))
                tree_struct[key]['children_names'].append(sub_dir)
                num_child += 1
    memory = dict()
    for key, values in tree_struct.items():
        if not values['children']:
            memory[key] = values['memory']
    while len(memory) != len(tree_struct):
        for key, values in tree_struct.items():
            if key not in memory:
                mem = values['memory']
                missing = False
                for sub_key in values['children']:
                    if sub_key in memory:
                        mem += memory[sub_key]
                    else:
                        missing = True
                if not missing:
                    memory[key] = mem
    tot = 0
    for key, value in memory.items():
        if value <= 100000:
            tot += value
            # Part 1
    if problem == 1:
        return tot
    # Part 2
    elif problem == 2:
        tot_space = 70000000
        needed_space = 30000000
        space_to_be_removed = memory[(0, )] - (tot_space - needed_space)
        smallest_to_be_removed = memory[(0, )]
        for key, val in memory.items():
            if val >= space_to_be_removed:
                if val < smallest_to_be_removed:
                    smallest_to_be_removed = val
        return smallest_to_be_removed
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
