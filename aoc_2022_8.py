#
# Solution to Advent of Code 2002 Day 8 - Treetop Tree House
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
import numpy as np

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the eight day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def get_scenic_score(trees, row, col):
    height = trees[row, col]
    # Left
    view = True
    left = 0
    x_pos = col - 1
    y_pos = row
    while view:
        if x_pos >= 0:
            if trees[y_pos, x_pos] >= height:
                view = False
            left += 1
        else:
            view = False
        x_pos -= 1
    # Right
    view = True
    right = 0
    x_pos = col + 1
    y_pos = row
    while view:
        if x_pos <= len(trees) - 1:
            if trees[y_pos, x_pos] >= height:
                view = False
            right += 1
        else:
            view = False
        x_pos += 1
    # Up
    view = True
    up = 0
    x_pos = col
    y_pos = row - 1
    while view:
        if y_pos >= 0:
            if trees[y_pos, x_pos] >= height:
                view = False
            up += 1
        else:
            view = False
        y_pos -= 1
    # Down
    view = True
    down = 0
    x_pos = col
    y_pos = row + 1
    while view:
        if y_pos <= len(trees) - 1:
            if trees[y_pos, x_pos] >= height:
                view = False
            down += 1
        else:
            view = False
        y_pos += 1
    return left * right * up * down


def compute():
    with open(data_dir + '/aoc_2022_8.txt') as f:
        data = f.read().splitlines()
    # Part 1
    if problem == 1:
        dim = len(data[0])
        trees = np.empty([dim, dim])
        for i in range(dim):
            for j in range(dim):
                trees[i, j] = data[i][j]
        # Left visible
        left_visible = np.ones([dim, dim])
        for row in range(dim):
            highest = -1
            for col in range(dim):
                if trees[row, col] > highest:
                    highest = trees[row, col]
                else:
                    left_visible[row, col] = 0
        # Right visible
        right_visible = np.ones([dim, dim])
        for row in range(dim):
            highest = -1
            for col in reversed(range(dim)):
                if trees[row, col] > highest:
                    highest = trees[row, col]
                else:
                    right_visible[row, col] = 0
        # Upper visible
        upper_visible = np.ones([dim, dim])
        for col in range(dim):
            highest = -1
            for row in range(dim):
                if trees[row, col] > highest:
                    highest = trees[row, col]
                else:
                    upper_visible[row, col] = 0
        # Lower visible
        lower_visible = np.ones([dim, dim])
        for col in range(dim):
            highest = -1
            for row in reversed(range(dim)):
                if trees[row, col] > highest:
                    highest = trees[row, col]
                else:
                    lower_visible[row, col] = 0
        tot = upper_visible + lower_visible + left_visible + right_visible
        return np.count_nonzero(tot)
    # Part 2
    elif problem == 2:
        dim = len(data[0])
        trees = np.empty([dim, dim])
        for i in range(dim):
            for j in range(dim):
                trees[i, j] = data[i][j]
        scenic_score = np.zeros([dim, dim])
        for row in range(dim):
            for col in range(dim):
                scenic_score[row, col] = get_scenic_score(trees, row, col)
        return np.max(scenic_score)
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
