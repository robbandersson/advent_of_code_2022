#
# Solution to Advent of Code 2002 Day 9 - Rope Bridge
#
# https://gitlab.com/robbandersson/advent_of_code_2022
#
import os
import argparse
import numpy as np
import re

data_dir = os.path.normpath("data")
parser = argparse.ArgumentParser(description='Solves the ninth day\'s problem')
parser.add_argument('part', metavar='part', type=int, help='Enter which part you want to solve')
args = parser.parse_args()
problem = args.part


def update_tail_pos(head_pos, tail_pos):
    if (abs(head_pos[0] - tail_pos[0]) > 1) and (abs(head_pos[1] - tail_pos[1]) > 1):
        tail_pos = (tail_pos[0] + np.sign(head_pos[0] - tail_pos[0]),
                    tail_pos[1] + np.sign(head_pos[1] - tail_pos[1]))
    elif abs(head_pos[0] - tail_pos[0]) > 1:
        if abs(head_pos[1] - tail_pos[1]) == 1:
            tail_pos = (tail_pos[0] + np.sign(head_pos[0] - tail_pos[0]),
                        tail_pos[1] + np.sign(head_pos[1] - tail_pos[1]))
        else:
            tail_pos = (tail_pos[0] + np.sign(head_pos[0] - tail_pos[0]), tail_pos[1])
    elif abs(head_pos[1] - tail_pos[1]) > 1:
        if abs(head_pos[0] - tail_pos[0]) == 1:
            tail_pos = (tail_pos[0] + np.sign(head_pos[0] - tail_pos[0]),
                        tail_pos[1] + np.sign(head_pos[1] - tail_pos[1]))
        else:
            tail_pos = (tail_pos[0], tail_pos[1] + np.sign(head_pos[1] - tail_pos[1]))
    return tail_pos


def compute():
    # with open(data_dir + '/aoc_2022_9_test_2.txt') as f:
    with open(data_dir + '/aoc_2022_9.txt') as f:
        data = f.read().splitlines()
    # Part 1
    if problem == 1:
        head_pos = (0, 0)
        tail_pos = (0, 0)
        visited = [tail_pos]
        for row in data:
            direction = row[0]
            steps = int(re.findall(r'[0-9]+', row)[0])
            if direction == 'L':
                for step in range(steps):
                    head_pos = (head_pos[0], head_pos[1] - 1)
                    tail_pos = update_tail_pos(head_pos, tail_pos)
                    visited.append(tail_pos)
            elif direction == 'R':
                for step in range(steps):
                    head_pos = (head_pos[0], head_pos[1] + 1)
                    tail_pos = update_tail_pos(head_pos, tail_pos)
                    visited.append(tail_pos)
            elif direction == 'U':
                for step in range(steps):
                    head_pos = (head_pos[0] + 1, head_pos[1])
                    tail_pos = update_tail_pos(head_pos, tail_pos)
                    visited.append(tail_pos)
            else:
                for step in range(steps):
                    head_pos = (head_pos[0] - 1, head_pos[1])
                    tail_pos = update_tail_pos(head_pos, tail_pos)
                    visited.append(tail_pos)
        return len(set(visited))
    # Part 2
    elif problem == 2:
        positions = {0: (0, 0), 1: (0, 0), 2: (0, 0), 3: (0, 0), 4: (0, 0),
                     5: (0, 0), 6: (0, 0), 7: (0, 0), 8: (0, 0), 9: (0, 0)}
        visited = []
        for row in data:
            direction = row[0]
            steps = int(re.findall(r'[0-9]+', row)[0])
            if direction == 'L':
                for step in range(steps):
                    positions[0] = (positions[0][0], positions[0][1] - 1)
                    for k in range(1, 10):
                        positions[k] = update_tail_pos(positions[k - 1], positions[k])
                    visited.append(positions[9])
            elif direction == 'R':
                for step in range(steps):
                    positions[0] = (positions[0][0], positions[0][1] + 1)
                    for k in range(1, 10):
                        positions[k] = update_tail_pos(positions[k - 1], positions[k])
                    visited.append(positions[9])
            elif direction == 'U':
                for step in range(steps):
                    positions[0] = (positions[0][0] + 1, positions[0][1])
                    for k in range(1, 10):
                        positions[k] = update_tail_pos(positions[k - 1], positions[k])
                    visited.append(positions[9])
            else:
                for step in range(steps):
                    positions[0] = (positions[0][0] - 1, positions[0][1])
                    for k in range(1, 10):
                        positions[k] = update_tail_pos(positions[k - 1], positions[k])
                    visited.append(positions[9])
        return len(set(visited))
    else:
        return 'Select either part 1 or 2'


if __name__ == '__main__':
    print(compute())
